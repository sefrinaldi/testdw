<?php
    include_once("config.php");

    $result = mysqli_query($mysqli, "SELECT * FROM users ORDER BY id DESC");

    if(isset($_POST['login'])){

        $name = $_POST['name'];
        $email = $_POST['email'];
    
        $result = mysqli_query($mysqli, "SELECT * FROM users WHERE name=$name and email=$email");
        
    }
?>

<html>
    <head>
        <title>Login</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </head>

    <body>
        <div class="container col-sm-3">
            <div class="card text-center">
                <div class="card-header">
                    <h2>Login</h2>
                </div>

                <div class="card-body">
                    <form action="login.php" method="post" name="form1">
                        <table>
                            <tr>
                                <td width="30%"><label for="name">Nama</label></td>
                                <td><input type="text" class="form-control" id="name" name="name"></td>                    
                            <tr>
                            <tr>
                                <td><label for="email">Email</label></td>
                                <td><input type="email" class="form-control" id="email" name="email"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <button type="submit" class="btn btn-primary" name="login">Login</button>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>