<?php
include_once("config.php");

if(isset($_POST['update']))
{   
    $id = $_POST['id'];

    $name=$_POST['name'];
    $email=$_POST['email'];

    $result = mysqli_query($mysqli, "UPDATE users SET name='$name',email='$email' WHERE id=$id");

    header("Location: add.php");
}
?>

<?php
$id = $_GET['id'];

$result = mysqli_query($mysqli, "SELECT * FROM users WHERE id=$id");

while($user_data = mysqli_fetch_array($result))
{
    $name = $user_data['name'];
    $email = $user_data['email'];
}
?>

<html>
    <head>  
        <title>Edit User Data</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </head>

    <body>        
        <div class="card">
            <div class="card-body">
                <a href="add.php" class="btn btn-primary">Back</a>
                <br/><br/>

                <form name="update_user" method="post" action="edit.php">
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control col-sm-4" id="name" name="name" value=<?php echo $name;?>>                        
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control col-sm-4" id="email" name="email" value=<?php echo $email;?>>
                    </div>  
                    <div>
                        <input type="hidden" name="id" value=<?php echo $_GET['id'];?>>
                        <button type="submit" class="btn btn-primary" name="update">Update</button>
                    </div>                  
                    
                </form>                
            </div>   
        </div>
    </body>
</html>