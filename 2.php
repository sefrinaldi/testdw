<?php

$huruf = ["a", "b", "c", "d", "e"];

function putarArray($huruf)
{
    $isi = $huruf[0];
    unset($huruf[0]);
    $huruf[] = $isi;
    
    return $huruf;
}

$huruf = putarArray($huruf);

echo "Putaran 1 : ";
foreach($huruf as $row){
    echo "$row ";
}

?>