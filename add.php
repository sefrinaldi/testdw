<?php
    include_once("config.php");

    $result = mysqli_query($mysqli, "SELECT * FROM users");
?>

<html>
    <head>
        <title>Add Users</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </head>

    <body>
        <div class="card">
            <div class="card-body">
                <a href="index.php" class="btn btn-primary">Back</a>
                <br/><br/>

                <form action="add.php" method="post" name="form1">
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control col-sm-4" id="name" name="name">                        
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control col-sm-4" id="email" name="email">
                    </div>                    
                    <button type="submit" class="btn btn-primary" name="Submit">Add</button>
                </form>                
            </div>     

            <div class="card-body"> 
                <table class="table table-bordered">
                    <thead>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                    <?php  
                        while($user_data = mysqli_fetch_array($result)) {         
                            echo "<tr>";
                            echo "<td>".$user_data['name']."</td>";
                            echo "<td>".$user_data['email']."</td>";    
                            echo "<td><a href='edit.php?id=$user_data[id]'>Edit</a> | <a href='delete.php?id=$user_data[id]'>Delete</a></td></tr>";        
                        }
                    ?>
                    </tbody>
                </table>             
            </div>               
        </div>

        <?php
        if(isset($_POST['Submit'])) {
            $name = $_POST['name'];
            $email = $_POST['email'];        

            include_once("config.php");

            $result = mysqli_query($mysqli, "INSERT INTO users(name,email) VALUES('$name','$email')");

            echo "User berhasil ditambahkan.";
            header("Location: add.php");
        }
        ?>
    </body>
</html>