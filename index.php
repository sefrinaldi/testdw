<?php
    include_once("config.php");

    $result = mysqli_query($mysqli, "SELECT * FROM image_blog");
?>

<html>
    <head>
        <title>Test DW No 4</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </head>

    <body>
    <div class="card">
        <div class="card-body">
            <nav class="navbar navbar-light bg-light justify-content-between">
            <h1>Navbar</h1>
            <form class="form-inline">
                <a href="add_image.php" class="btn btn-info">Add Image Blog</a>&nbsp;&nbsp;
                <a href="add.php" class="btn btn-info">Add User</a>            
            </form>
            </nav>
        </div>    

        <?php  
            while($image_data = mysqli_fetch_array($result)) { ?>        
                <div class="card" style="width: 18rem;">
                    <img class="img img-thumbnail" src="<?php echo $image_data['file_image']; ?>" width="150px">
                    <div class="card-body">
                        <p class="card-text"><?php echo $image_data['content']; ?></p>
                        <p class="card-text"><?php echo $image_data['title']; ?></p>
                        <a href="" class="btn btn-info">View Detail</a>
                    </div>
                </div>         
            <?php 
            } 
        ?>
    </div>
    </body>
</html>