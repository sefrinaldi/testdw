<html>
    <head>
        <title>Add Image</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </head>

    <body>
        <div class="card">
            <div class="card-body">
                <a href="index.php" class="btn btn-primary">Back</a>
                <br/><br/>

                <form action="" method="post" name="form1" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control col-sm-4" id="title" name="title">                        
                    </div>
                    <div class="form-group">
                        <label for="content">Content</label>
                        <input type="content" class="form-control col-sm-4" id="content" name="content">
                    </div> 
                    <div>
                        <label for="image">Image</label>
                        <input type="file" class="form-control col-sm-4" id="image" name="image">
                    </div>                   
                    <button type="submit" class="btn btn-primary" name="Submit">Add</button>
                </form> 
            </div>
        </div>

        <?php
        if(isset($_POST['Submit'])) {
            $title = $_POST['title'];
            $content = $_POST['content']; 
            $temp = $_FILES['image']['tmp_name'];  
            $name = rand(0,9999).$_FILES['image']['name']; 
            $folder = "gambar/"; 
            move_uploaded_file($temp, $folder . $name);   

            include_once("config.php");

            $result = mysqli_query($mysqli, "INSERT INTO image_blog(title,content,file_image,user_id) VALUES('$title','$content','$name', 1)");

            echo "User berhasil ditambahkan.";
            header("Location: add_image.php");
        }
        ?>
    </body>
</html>